package com.viventiumProject;
import com.viventiumProject.pages.HomePage;
import com.viventiumProject.pages.PageBase;
import com.viventiumProject.utils.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ScriptBase {

    protected WebDriver driver;
    public PageBase pageBase;
    protected HomePage homePage;


    @BeforeTest
    public void beforeTest() throws IOException, InterruptedException {

    }
    //chrome,chromeHeadless,firefox,ie,grid_chrome_16,grid_firefox_16,grid_ie_16
    @BeforeMethod(alwaysRun = true)
    @Parameters({"browserName", "env"})
    public void beforeMethod(@Optional(value = ("chrome")) String browserName, @Optional(value = ("qa")) String env) throws InterruptedException, IOException {

        driver = DriverFactory.getInstance(browserName).getDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10,TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();

       // pageBase = new PageBase();
        homePage = new HomePage(driver);
        driver.get("https://www.yahoo.com/");

    }

    // Step 11. Close the browser
    @AfterTest
    public void tearDown() {

       // driver.close();
//        driver.quit();
    }

    @AfterMethod
    public void afterClass() {
        driver  =null;
        homePage = null;
        pageBase= null;

      //  DriverFactory.getInstance().removeDriver();
    }


}
