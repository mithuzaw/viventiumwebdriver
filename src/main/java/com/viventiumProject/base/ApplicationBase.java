package com.viventiumProject.base;
import com.viventiumProject.pages.HomePage;
import com.viventiumProject.pages.PageBase;
import org.openqa.selenium.WebDriver;

public class ApplicationBase {
    protected WebDriver driver;
    protected HomePage homePage;
    protected PageBase pageBase;

    public ApplicationBase(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage homePage(){
        if (homePage == null){
            homePage = new HomePage(driver);
        }
        return homePage;
    }
    public PageBase pageBase(){
        if (pageBase == null){
            pageBase = new PageBase(driver);
        }
        return pageBase;
    }
}
