package com.viventiumProject.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class HomePage extends PageBase {

    @FindBy(xpath = "//button[@id='header-desktop-search-button']")
    protected WebElement searchBar;


    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
    }


    public void searchDifferentState(String state) {
        isSignInLinkDisplayed();
        searchBar.click();
        delayFor(2000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('header-search-input').value='" + state + "';");
    }
    public boolean isSignInLinkDisplayed() {
        return isElementDisplayed(searchBar,20);
    }

}
