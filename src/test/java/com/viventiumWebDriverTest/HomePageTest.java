package com.viventiumWebDriverTest;
import com.viventiumProject.ScriptBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import java.util.List;
/**
 * Created by Mithu on 8/28/20.
 */
public class HomePageTest extends ScriptBase {

    @Test
    public void testYahooPage() throws Exception {

        // Step 1. Assert that we are on the correct page by checking that title = 'Yahoo'
        Assert.assertEquals(driver.getTitle(), "Yahoo");

        // Step 2. Display the count of links under the search bar ('Mail', 'Coronavirus', 'News', 'Finance', ...)
        // including 'More...' option

        driver.findElement(By.xpath("//button[@id='header-desktop-search-button']")).click();
        Thread.sleep(2000);
        List<WebElement> searchBarList = driver.findElements(By.xpath("//div[@id='header-nav-bar-wrapper']//a"));
        System.out.println("Total count of links under the search bar -> " + searchBarList.size());

        // Step 3. Write a loop that will print each of the links from step 2

        for (WebElement element1 : searchBarList) {
            System.out.println("Link header bar --> " + element1.getText());
        }

        // Step 4. Enter 'New York City' in the search bar on the top

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('header-search-input').value='New York City';");

        // Step 5. Click Search button
        driver.findElement(By.xpath("//button[@id='header-desktop-search-button']")).click();

        // Step 6. Set a 7 second explicit wait for the result of the search
        // Use an appropriate condition of your choice

        WebDriverWait waitForElement = new WebDriverWait(driver, 700);
        WebElement elementToVisible = waitForElement.until(ExpectedConditions.visibilityOfElementLocated(By.id("sbq-wrap")));
        boolean status = elementToVisible.isDisplayed();

        if (status) {
            System.out.println("*****************************");
            System.out.println("New York is displayed");
        } else {
            System.out.println("New York is not displayed");
        }

        // Step 7. Click 'Sign In' button on the top right side
        driver.findElement(By.id("ysignin")).click();

        // Step 8. Display the boolean state of the checkbox next to 'Keep me signed in'
        boolean checkBox = driver.findElement(By.id("persistent")).isDisplayed();
        if (checkBox = false) {
            System.out.println(checkBox);
        } else {
            System.out.println("Keep me signed in");
        }

        // Step 9. Create a logic that will uncheck the checkbox if it is checked

        WebElement element = driver.findElement(By.id("persistent"));
        String checkBoxState = element.getAttribute("checked");
        if (checkBoxState != null && checkBoxState.contentEquals("true")) {
            js.executeScript("arguments[0].click();", element);
        }
    }

    /*
        **Bonus**
        You have the option to parameterize step 4 using DataProvider annotation or reading data data from excel sheets.
        Setup the script to search for 'New York City', 'San Diego', and 'Chicago'.
        */

    @DataProvider(name = "dataProviderAsArray")
    public Object[][] testYahooPageWithDataProvider() {
        Object[][] searchBarData;
        searchBarData = new Object[][]{
                {"New York City"},
                {"San Diego"},
                {"Chicago"}
        };
        return searchBarData;
    }

    @Test(dataProvider = "dataProviderAsArray")
    public void useCase_dataProviderForSearchBar(String stateName){

        driver.navigate().to("https://www.yahoo.com");
        homePage.searchDifferentState(stateName);
    }

}
